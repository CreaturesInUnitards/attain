/* globals Proxy, Symbol */
import * as S from './stream.js'
import { $ as Query } from './queries.js'

const Z = ({
  stream: theirStream2
  , query: $=Query

  // optional custom stream
  , read= () => theirStream2()
  , write= f => theirStream2( f(theirStream2()) )
  , notify= f => S.watch( $() ) (theirStream2).map(f)
}) => {

  const ourStream = S.of()
  const removed = S.of()

  ourStream.deleted = removed

  let lastGet

  let ignoreNotification = false

  const over = f => {
    ignoreNotification = true
    write( $(f) )

    return lastGet = ourStream( $() (read())[0] )
  }

  const throttled = ms =>
    S.afterSilence(ms) (S.dropRepeats(ourStream))

  const get = () =>
    [ read() ]
    .map( $() )
    .map(
      results => {
        if( results.length ) {
          // async redraw can mean a view is using Z.get()
          // to retrieve a list item that doesn't exist anymore
          // so we cache the last get
          lastGet = results[0]
          return lastGet
        } else {
          return lastGet
        }
      }
    )
    .shift()

  notify( () => {
    if( !ignoreNotification ) {
      ourStream( get() )
    }
    ignoreNotification = false
  })

  const remove = () => {
    const existing = get()

    write( $($.$delete) )

    removed( existing )
  }

  function prop(...args){
    if( args.length ) {
        if( typeof args[0] == 'function' ) {
            return over(...args)
        } else {
            return over( () => args[0] )
        }
    }
    return get()
  }

  function query(visitor){
    const query = visitor($)

    const z = Z({
      stream: theirStream2
      , read
      , write
      , query
    })

    return z
  }

  let others = {
    delete: remove
    , deleted: removed
    , stream: ourStream
    , query
    , throttled
    , filter: f => query( x => x.$filter(f) )
    , flatMap: f => query( x => x.$flatMap(f) )
    , get values(){
      return query( x => x.$values )
    }

    // add methods to the Z instance
    // todo-james make router use this instead of
    // the custom proxy
    , instance(f){
      others = f(others)
      return out
    }
  }

  const out = new Proxy(prop, {
    get(_, theirKey){
      const key =
        typeof theirKey == 'string'
        && /^\$\d+$/.test(theirKey)
        ? theirKey.slice(1)
        : theirKey

      if ( key == Symbol.toPrimitive ) {
        return toString
      } else if( typeof key == 'string' ){
        if( key == '$') {
          return query
        } else if( key.startsWith('$') ) {
          return others[key.slice(1)]
        } else {
          return query( x => x[key] )
        }
      } else {
        return others[key]
      }
    }
  })

  return out
}

export default Z