import { valueInstance} from './stags/index.js'

export default f => (...args) => {
    try { 
        const value = f(...args)
        if( value == null ) {
            throw new TypeError('Nil Result in encased function:' + f.toString())
        } else {
            return valueInstance('stags.Either', 'Y', value)
        }
    } catch (value) {
        return valueInstance('stags.Either', 'N', value)
    }
}