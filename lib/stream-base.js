
export const dropRepeatsWith = eq => s => {
    const sentinel = {}
    let prev = sentinel
    const out = stream()
  
    s.map(
      x => {
        if ( prev === sentinel || !eq(x, prev) ) {
          prev = x
          out(x)
        }
        return null
      }
    )
  
    return out
  }
  
  export const interval = ms => {
    const out = stream()
  
    const id = setInterval(
      () => out(Date.now())
      , ms
    )
  
    out.end.map(
      () => clearInterval(id)
    )
  
    out(Date.now())
    return out
  }
  
  
  export const raf = () => {
    let last = Date.now()
    let running = true
    const out = stream()
  
    function loop(){
      if (running){
        const dt = Date.now() - last
        out({ dt })
        last = Date.now()
  
        requestAnimationFrame(loop)
      }
    }
  
    requestAnimationFrame(loop)
  
    out.end.map( () => running = false )
  
    return out
  }
  
  export const afterSilence = ms => s => {
    let id
  
    const out = stream()
    s.map(
      x => {
        clearTimeout(id)
        id = setTimeout(
          () => out(x), ms
        )
        return null
      }
    )
  
    return out
  }
  
  export const throttle = ms => s => {
  
    const out = stream()
    let last = Date.now()
    let id = 0
  
    function process(x){
      let dt = Date.now() - last
  
      if( dt >= ms ){
        clearTimeout(id)
        out(x)
        last = Date.now()
      } else {
        id = setTimeout(process, Math.max(0, ms - dt), x )
      }
    }
  
    s.map(process)
  
    return out
  }
  
  export const dropRepeats = s =>
    dropRepeatsWith( (a, b) => a === b)(s)
  
  export const watch = f => s =>
    dropRepeats(s.map(f))
  
  export const filter = f => s => {
    const out = stream()
  
    s.map(
      x => f(x) ? out(x) : null
    )
  
    return out
  }
  
  export const map = f => s => s.map(f)
  export const decide = f => s => {
    const out = stream()
    s.map( f(out) )
    return out
  }
  
  export const async = f => s => {
    const out = stream()
    s.map( x => f(x).then(out) )
    return out
  }
  
  export const decideLatest = f => s => {
    let latest
    const out = stream()
  
    s.map(
      x => {
        latest = {}
        const mine = latest
        f(
          decision => {
            if(mine == latest) {
              out(decision)
            }
          }
        ) (x)
  
        return null
      }
    )
  
    return out
  }
  
  export const funnel = xs => {
    const out = stream()
  
    xs.map( s => s.map(
      // mithril doesn't seem to queue burst writes
      // so this does that for them
      // can't seem to repro this out of the app
      x => setTimeout(out, 0, x)
    ))
  
    return out
  }
  
  export const sink = s => {
    const out = stream()
  
    out.map(s)
  
    return out
  }
  
  export const source = s => {
    const out = stream()
  
    s.map(out)
  
    return out
  }
  
  export const scan = seed => f => s => stream.scan(f, seed, s)
  
  export const of = stream
  
  export const merge = xs => stream.merge(xs)
  
  export const log = o => Object.entries(o).forEach(
    ([k,v]) => v.map( x => console.log(k, x ))
  )
  
  export const session = () => {
  
    const session = []
    const of = (...args) => {
      const out = stream(...args)
      session.push(out)
      return out
    }
  
    const end = () => {
      session.map( s => s.end(true) )
    }
  
    return { of, end }
  }
  
  export default stream