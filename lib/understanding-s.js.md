# Understanding S.js

S.js is one of the fastest stream libraries around.  It was written by Adam Haile in 2003 and has inpsired a lot of interesting work in UI and Reactive programming.  It however uses a very different model to most stream libraries, users of knockout.js may find it familiar, stream relationships/dependencies are automatically detected by usage.

I'm part of the team working on a new UI library: Vella.  And Vella will be largely powered by S.js.  As a team we agreed there's some rough edges of S.js that may or may not be required for our use case.  We also agreed, it is important to understand the S.js source back to front so we can responsibly maintain our library which depends on S for all of its core operations.

So this post's goal is to document the process of understanding the codebase with very little pre-existing knowledge of the space or ecosystem.  Thankfully, it is just one file, and it is only ~700 odd lines of code.  So it is doable!  Let's begin.

## Usage API

Before we look at the internals let's look at the external API and imagine how it might work behind the scenes.  That way we can reference any unanswered questions or mysteries later on.

You can create an event stream in S using the `S.data` constructor.  The resulting stream is a getter/setter.

```js
const name = S.data('James')

name() // 'James'

name('Fred')

name() // Fred
```

So far, not _too_ disimilar to other stream libraries like `flyd` or `mithril-stream`.

We can also subscribe to a stream's changes a few different ways, let's start with what might be the most familiar.


```js
const name = S.data('James')

S.on(name, () => console.log('Name:', name() ))

name('Fred')
name('Barney')
name('Pierre-Yves')

// logs
// Name: James
// Name: Fred
// Name: Barney
// Name: Pierre-Yves
```

We can listen to multiple streams at once using `on` as well because, `S.on` accepts a list.


```js
const name = S.data('James')
const age = S.data(10000)

S.on([name, age], () => console.log('Name:', name(), 'Age:', age() ))

name('Fred')
name('Barney')
age(10001)
name('Pierre-Yves')

// logs
// Name: James Age: 10000
// Name: Fred Age: 10000
// Name: Barney Age: 10000
// Name: Barney Age: 10001
// Name: Pierre-Yves Age: 10001
```

This is still familiar territory hopefully.

But what if we want to subscribe to a stream and produce a value instead of just logging or performing a side effect?

S calls this a `computation`.  Turns out, all we need to do is return a value and store the resulting stream.

```js

const name = S.data('James')
const age = S.data(10000)

// Stream<[String, String, String, Number]>
const log = 
    S.on([name, age], () => ['Name:', name(), 'Age:', age() ])

S.on(log, () => console.log(...log()))

name('Fred')
name('Barney')
age(10001)
name('Pierre-Yves')

// logs
// Name: James Age: 10000
// Name: Fred Age: 10000
// Name: Barney Age: 10000
// Name: Barney Age: 10001
// Name: Pierre-Yves Age: 10001
```

At this point, you might be wondering why `S.on` doesn't pass in the values of the streams `name` and `age` like e.g. `stream.map` would in other libraries.  That's because S automatically detects subscriptions, and so instead of `S.on` we can simply use `S` like so:


```js

const name = S.data('James')
const age = S.data(10000)

// Automatically tracks that `log` depends on `name` and `age`
// S.on doesn't automatically track dependencies and can be used
// for some edge cases where automatic dependency tracking could
// cause problems.
const log = 
    S(() => ['Name:', name(), 'Age:', age() ])

// When log emits, our S function will execute
// and console.log will run.
S(() => console.log(...log()))

name('Fred')
name('Barney')
age(10001)
name('Pierre-Yves')

// logs
// Name: James Age: 10000
// Name: Fred Age: 10000
// Name: Barney Age: 10000
// Name: Barney Age: 10001
// Name: Pierre-Yves Age: 10001
```

Both `S` and `S.on` return a computation stream.  The difference is that `S` automatically tracks dependencies.  In general, it's idiomatic to prefer the usage of `S` over `S.on`.

`S` and `S.on` can also take an initial value, which can let us reproduce a scan of a stream like so.

```js
const num = S.data()
const sum = S( x => x + num(), 0)

sum() // 0
num(1)
sum() // 1 (0 + 1)
num(2)
sum() // 3 (1 + 2)
num(3)
sum() // 6 (3 + 3)
```

Other libraries might use a method like:

```js
stream.scan( 
    (previous, next) => previous + next
    , seed
    , stream 
)
```

Because in S it is idiomatic to automatically track dependencies, we both do not need the `next` value, and we don't need the `stream` argument.  Which is why the signatures are so different even if the result behaviour is the same.

---

Now if you were following along in a REPL, you may have seen an error message warning that without a `root` any created streams will never be cleaned up.  This is one of those magic rough edges I mentioned in the beginning.  What is a `root`?  And how do you clean up streams in `S`?

Well `S` has a really cool super power that other stream libraries tend not to have.  Because stream usage is automatically tracked, stream's can also be automatically cleaned up when the parent context is disposed.  And because the parent context follows the same rules: all streams are automatically cleaned up.  But there's one caveat.  What if there is no parent context?  If there's no parent context you can't have automatic clean up.

Enter `S.root`.

```js
S.root( dispose => {
    const name = S.data('James')

    setTimeout( () => {
        // dispose this stream context in 10 seconds.
        dispose()
    }, 10000 )
})
```

`S.root` fills the need of a top level parent context for top level streams and computations.  Now - I'm not sure if `root` is really necessary.  And `root` and the cleanup mechanism, at the time of writing this sentence, still kind of mystifies me.  But by the end of this post we'll have figured out exactly the requirements, mechanisms and tradeoffs of `S.root` and automatic clean up.

Speaking of `cleanup`, S does automatically handle it, _but_ we can subscribe to the cleanup phase via `S.cleanup`.


```js
S( () => {
    name()

    S.cleanup( lastTime => {
        if( lastTime ){
            console.log('name stream is being cleaned up')
        }
    })
})
```

This let's us deregister event listeners, or do some other kind of clean up side effect when a stream is about to be GC'd.

There are a few other mechanisms and methods on the S API, but that's a pretty good walkthrough of the core functionality, enough for us to get started.

## The Source Code

There's not an exact methodology we are going to use to explore the source code.  But a high level process will be, we start top to bottom, as we encounter something that doesn't make sense, we explore it immediately.  When we finally understand that current mystery, we go back to our original place in the top to bottom scan.

If a long the way, we find additional mysteries or points of confusion, we add them to our stack of things to search.  We'll explore things on that stack before returning to our scan.

That way, we're building up mastery.  We won't proceed until we've understand (to some degree) a particular subsection, or abstraction.  We're going to be slow but thorough.

The code will be reference for this post is on the project's github [here](https://github.com/adamhaile/S/blob/634e5fb5c1a6726a8eaf0ade1d13a719effea3ed/src/S.ts).  We're linking to a specific git ref, so that when the code updates this blog post will not get out of sync ith the referenced code.

### Type Exports

The top of the file starts with some type exports.  This is a pretty common practice in type driven design.  If we explore and understand these types before reading the source, hopefully we can have a better grasp of the following code.  The types are a way to read the intention of the author and the library.

Here is the entire interface.

```ts

export interface S {
    // Computation root
    root<T>(fn : (dispose? : () => void) => T) : T;

    // Computation constructors
    <T>(fn : () => T) : () => T;
    <T>(fn : (v : T) => T, seed : T) : () => T;
    on<T>(ev : () => any, fn : () => T) : () => T;
    on<T>(ev : () => any, fn : (v : T) => T, seed : T, onchanges?: boolean) : () => T;
    effect<T>(fn : () => T) : void;
    effect<T>(fn : (v : T) => T, seed : T) : void;

    // Data signal constructors
    data<T>(value : T) : DataSignal<T>;
    value<T>(value : T, eq? : (a : T, b : T) => boolean) : DataSignal<T>;

    // Batching changes
    freeze<T>(fn : () => T) : T;

    // Sampling a signal
    sample<T>(fn : () => T) : T;

    // Freeing external resources
    cleanup(fn : (final : boolean) => any) : void;

    // experimental - methods for creating new kinds of bindings
    isFrozen() : boolean;
    isListening() : boolean;
    makeDataNode<T>(value : T) : IDataNode<T>;
    makeComputationNode<T, S>(fn : (val : S) => T, seed : S, orphan : boolean, sample : true) : { node : INode<T> | null, value : T };
    makeComputationNode<T, S>(fn : (val : T | S) => T, seed : S, orphan : boolean, sample : boolean) : { node : INode<T> | null, value : T };
    disposeNode(node : INode<any>) : void;
}
```
Let's work through this bit by bit.

```ts
root<T>(fn : (dispose? : () => void) => T) : T;
```

This signature just means, root is a function that accepts a function.  The `<T>` `=> T` and `:T` means, whatever the function `fn` returns, root will _also_ return a value of the same type.

So `S.root( () => 0)` would return `0`, because the `fn` `() => 0` returned `0`.

Why is that useful?  I'm not sure.

Next up computation constructors, let's start with the variations of `S( () => ... )`

```ts
<T>(fn : () => T) : () => T;
<T>(fn : (v : T) => T, seed : T) : () => T;
```

Let's start with the end there, the `: () => T` means, this expression returns a function that returns type `T`.  That is our getter function mentioned earlier.

Our `S` expression accepts a function `fn`, which simply returns a value of type `T`.

The second signature just says, you can also provide a `seed` value, that must be of the same type `T` than `fn` returns.

This is all in line with the usage code we've seen before.

```js
S( x => x + num(), 0)
```

`num` is a number stream, being added to `x`, also a `number` and `S` is taking a seed value of `number`.  So our type `T` is `number`, and the type signature enforces all those types need to line up - which is a good thing.  That will avoid a lot of common bugs e.g.

```js
1 + "0" // "10"
```

The `S.on` signatures are very similar except they accept a list of streams and there is a `onchanges` boolean I didn't mention earlier.  `onchanges` is just an option that forces `on` to only react to subsequent events, not the existing value in the referenced streams.

```ts
on<T>(ev : () => any, fn : () => T) : () => T;
on<T>(ev : () => any, fn : (v : T) => T, seed : T, onchanges?: boolean) : () => T;
```

Note, our `ev` is a function that returns `any`.  That's not really accurate, that's just the type system not being sufficient to model the behaviour of `on` so they are default to `any`.  This isn't taking into account that `ev` could be a list of streams of different types.

This isn't a huge deal, but is common in typescript code, you can't accurately or easily model a lot of common scenarios in Javascript, like the type of a list that represents a a dynamic tuple.  Maybe it is possible in the future or even now with a special incantantion.  But it's not simple.  So we tend to see `any` appear.  `any` just tells typescript to accept any value as valid.

`effect` is next, we didn't cover that yet in the usage guide.  It is not part of the public documentation, so we won't spend much time on it unless it becomes important to understand later usage code.


But it looks like `S` and `effect` have the exact same type signature _except_ there is no returned stream.  We can infer from these signatures that `effect` is just calling `S` internally and not returning the result.  Let's have a look at the types, and then the source.

```ts
      <T>(fn : () => T) : () => T; // S(...)
effect<T>(fn : () => T) : void;

      <T>(fn : (v : T) => T, seed : T) : () => T; // S( ... )
effect<T>(fn : (v : T) => T, seed : T) : void;
```

And here is the source for `effect`.

```ts
S.effect = function effect<T>(fn : (v : T | undefined) => T, value? : T) : void {
    makeComputationNode(fn, value, false, false);
}
```

And the source for `S`

```ts
// Public interface
var S = <S>function S<T>(fn : (v : T | undefined) => T, value? : T) : () => T {
    // ...

    var { node, value: _value } = makeComputationNode(fn, value, false, false);

    if (node === null) {
        return function computation() { return _value; }
    } else {
        return function computation() {
            return node!.current();
        }
    }
};
```

So `S` internally calls `makeComputationNode` and returns a getter.  `effect` internally calles `makeComputationNode` but doesn't return a getter.  So we can see we can definitely make reasonable inferences about source code by simply reading types.

We'll return to `S` source after dissecting the types.

---

Next comes the `data` and `value` constructors.

```ts
// Data signal constructors
data<T>(value : T) : DataSignal<T>;
value<T>(value : T, eq? : (a : T, b : T) => boolean) : DataSignal<T>;
```

I didn't cover `S.value` yet, but `S.value` is just `S.data` that diffs event values and only emits if the value changed.  `S.value` also offers an optional equality function `eq`.  Note they both return a `DataSignal<T>`.  From that we can assume value internally calls the same or shared code as data, but adds some additional diffing logic.

But what is a `DataSignal`

As far as the types are concerned, it is a getter setter function.

```ts
export interface DataSignal<T> {
    () : T;
    (val : T) : T;
}
```

---

```ts
// Batching changes
freeze<T>(fn : () => T) : T;

// Sampling a signal
sample<T>(fn : () => T) : T;
```