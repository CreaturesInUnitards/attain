import S from 's-js'

const Pending = 'Pending'
const Active = 'Active'

const HALT = {}
const SKIP = HALT

const combine = (combiner, xs) => {
  const out = of()

  S( () => {
    // trick S
    xs.map( f => f() )

    if( xs.every( x => x.tag == Active ) ) {
      const combined = combiner(...xs)

      if( combined != HALT ) {
        out(combined)
      }
    }
  })

  return out

}

const merge = xs => combine(
  (...xs) => xs.map( f => f() ), xs
)

const scan = (acc, seed, stream) => {
  const out = of(seed)

  S(() => {
    // listen to updates
    // stream.stream()
    // get value from _our_ api
    const next = stream()

    if( stream.tag == 'Active' ){

      const sum = acc(out.value, next)

      // tell the world about the change
      out( sum )
    }
  })

  return out
}

const of = (...args) => {
  const x = S.data(...args)

  const x1 = (...args) => {
    if( args.length > 0 ) {
      x1.value = args[0]
      x1.tag = Active
    }
    x(...args)
    return x1.value
  }

  if( args.length ){
    Object.assign(x1, Stream.Active(args[0]))
  } else {
    Object.assign(x1, Stream.Pending())
  }

  Object.assign(x1, Stream.proto)

  x1.stream = x

  return x1
}

const Stream = {
  type: 'Stream.of',
  tags: [Pending, Active],
  specs: {},
  Pending: () => ({ type: Stream.type, tag: Pending }),
  Active: value => ({ type: Stream.type, tag: Active, value }),
  proto: {
    map(f){
      return combine( x => f(x()), [this] )
    },
    ap(b){
      return combine( (a,b) => a()(b()), [this, b] )
    }
  }
  ,of
  ,combine
  ,HALT
  ,SKIP
  ,merge
  ,scan
  ,map(f,s){
    return s.map(f)
  }
}

Stream.proto['fantasyland/map'] = Stream.proto.map
Stream.proto['fantasyland/ap'] = Stream.proto.ap
Stream.proto['fantasyland/of'] = Stream.proto.of

export const dropRepeatsWith = eq => s => {
  const sentinel = {}
  let prev = sentinel
  const out = Stream()

  s.map(
    x => {
      if ( prev === sentinel || !eq(x, prev) ) {
        prev = x
        out(x)
      }
      return null
    }
  )

  return out
}

export const interval = ms => {
  const out = Stream()

  const id = setInterval(
    () => out(Date.now())
    , ms
  )

  S.cleanup(() => {
    clearInterval(id)
  })

  out(Date.now())
  return out
}


export const raf = () => {
  let last = Date.now()
  let running = true
  const out = Stream()

  function loop(){
    if (running){
      const dt = Date.now() - last
      out({ dt })
      last = Date.now()

      requestAnimationFrame(loop)
    }
  }

  requestAnimationFrame(loop)

  S.cleanup( () => {
    running = false
  })

  return out
}

export const afterSilence = ms => s => {
  let id

  const out = Stream()
  s.map(
    x => {
      clearTimeout(id)
      id = setTimeout(
        () => out(x), ms
      )
      return null
    }
  )

  return out
}

export const throttle = ms => s => {

  const out = Stream()
  let last = Date.now()
  let id = 0

  function process(x){
    let dt = Date.now() - last

    if( dt >= ms ){
      clearTimeout(id)
      out(x)
      last = Date.now()
    } else {
      id = setTimeout(process, Math.max(0, ms - dt), x )
    }
  }

  s.map(process)

  return out
}

export const dropRepeats = s =>
  dropRepeatsWith( (a, b) => a === b)(s)

export const watch = f => s =>
  dropRepeats(s.map(f))

export const filter = f => s => {
  const out = Stream()

  s.map(
    x => f(x) ? out(x) : null
  )

  return out
}

export const map = f => s => s.map(f)
export const decide = f => s => {
  const out = Stream()
  s.map( f(out) )
  return out
}

export const async = f => s => {
  const out = Stream()
  s.map( x => f(x).then(out) )
  return out
}

export const decideLatest = f => s => {
  let latest
  const out = Stream()

  s.map(
    x => {
      latest = {}
      const mine = latest
      f(
        decision => {
          if(mine == latest) {
            out(decision)
          }
        }
      ) (x)

      return null
    }
  )

  return out
}

export const funnel = xs => {
  const out = Stream()

  xs.map( s => s.map(
    // mithril doesn't seem to queue burst writes
    // so this does that for them
    // can't seem to repro this out of the app
    x => setTimeout(out, 0, x)
  ))

  return out
}

export const sink = s => {
  const out = Stream()

  out.map(s)

  return out
}

export const source = s => {
  const out = Stream()

  s.map(out)

  return out
}

export const log = o => Object.entries(o).forEach(
  ([k,v]) => v.map( x => console.log(k, x ))
)

export default Stream