import config from './rollup.template.js'

export default config({
    input: './lib/index.js',
    file: './dist/attain.min.js',
    name: 'attain'
})
