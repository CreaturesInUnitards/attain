export default () => {
    const data = require ('attain')

    const User = 
        data.tags('User', ['LoggedIn', 'LoggedOut'])
    
    const message = 
        User.fold({
            LoggedIn: ({ name }) => `Welcome ${name}!`,
            LoggedOut: () => `Goodbye!`
        })
        
    {
    [ message(User.LoggedIn({ name: "Bernie" })) // Hello Bernie!
    , message(User.LoggedOut()) // Goodbye!
    ]
    }
}