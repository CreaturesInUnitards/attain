Sum types are types that are the _sum_ of multiple structures.

Sum Types are _everyhere_ in programming but we often don't notice because the underlying language does not give us the tools out of the box to explore them.

If you're using booleans to track if some data has a particular type or shape you probably need sum types instead.

The sum types we offer are based on an extremely simple data structure that is serializable and easy to understand.

```js
{ type:string, tag:string, value?:any }
```

You can extend the base functionality as much or as little as you want using the `spec` system.  We use this system to implement helpers _and_ type classes like `functor`, `monad` and `bifunctor`.