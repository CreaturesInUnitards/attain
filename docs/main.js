import V from './lib/view.js'
import css from './css.js'

const { Route, state, v } =
    V(document.body, {
        Route: {
            Home: ['/', () => import('./home.js')],
            Docs: ['/docs', () => import('./docs.js')],
            Examples: ['/examples', () => import('./examples.js')],
        }
    })

// window.v = v
window.state = state
export { Route, state, css, v }