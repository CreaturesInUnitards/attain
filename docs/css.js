import css from 'bss'

css.setDebug(true)
css.helper('desktop', x =>
	css.$media('(min-width: 600px)', x)
)

css.helper('wide', x =>
	css.$media('(min-width: 1000px)', x)
)


document.body.classList.add(
	css`
		background: linear-gradient( rgba(0, 0, 0, 0.89), rgba(46, 78, 206, 0.67) ), url(https://images.unsplash.com/photo-1419242902214-272b3f66ee7a?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1574&q=80);
		color: white;
		background-position: bottom;
		background-repeat: no-repeat;
		background-color: black;
		background-size: cover;
		min-height: 100vh;
		margin: 0px;
		box-sizing: border-box;
		padding: 0.1em;
	`
	.desktop(`
		padding: 1em;
	`)
	.class
)

export default css