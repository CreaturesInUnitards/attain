import config from './rollup.template.js'

export default config({
    input: './lib/view.js',
    file: './dist/attain.view.min.js',
    name: 'v'
})
