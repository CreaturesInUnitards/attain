import test from 'tape'
import Z from '../lib/z.js'
import { $ } from '../lib/queries.js'
import * as stream from '../lib/stream.js'
import * as S from '../lib/stags/index.js'

test('Z', t => {

    const users = stream.of(S.Y([
        { id: 1, name: 'Tarek' },
        { id: 2, name: 'Sara' }
    ]))

    const query =
        $
            .$(S.map)
            .$values
            .$filter( x => x.id == 2 )

    const emissions = {
        users: [],
        name: [],
        name2: [],
        name3: []
    }

    const z = Z({
        read: () => users()
        ,write: f => users(f(users()))
        , stream: users
        , query: $.$(S.map)
    })

    const user = z.$values.$filter( x => x.id == 2 )

    const name = user.name
    const name2 = user.name
    const name3 = name.$( x => x )

    users.map( x => emissions.users.push( query.name() (x)[0] ) )
    name.$stream.map( x => emissions.name.push(x) )
    name2.$stream.map( x => emissions.name2.push(x) )
    name3.$stream.map( x => emissions.name3.push(x) )

    name('Sas')

    t.equals(
        query.name() (users())[0]
        , name()
    )

    name2('Sassafras')
    name3('Sarah')

    t.deepEquals(
        {
          users: [ 'Sara', 'Sas', 'Sassafras', 'Sarah' ]
          , name: [ 'Sara', 'Sas', 'Sassafras', 'Sarah' ]
          , name2: [ 'Sara', 'Sas', 'Sassafras', 'Sarah' ]
          , name3: [ 'Sara', 'Sas', 'Sassafras', 'Sarah' ]
        }
        ,emissions
        , 'Parent and children receive all change without infinite loops'
    )

    user.$delete()


    t.end()
})